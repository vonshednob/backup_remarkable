# Backup Remarkable

Just a set of tiny script for creating a backup of and cleaning up deleted 
files from your reMarkable 2 tablet. All from the command line.

Wait, cleanup deleted files? See [Background](#background) below for 
details.


## Requirements

 - The reMarkable 2 must be accessible through the filesystem (ie. mounted)
 - For the backup the USB web interface should be enabled
 - If you have [rmrl](https://github.com/rschroll/rmrl) installed, it will 
 be used (in which case the USB web interface is not required)

## Installation

Not really needed, just run `backup_remarkable.py` for the backup or use 
`cleanup_remarkable.py` to perform the cleanup.


## Usage

The general idea is that you mount the remarkable through something like
[sshfs](https://github.com/libfuse/sshfs) and then point the
`backup_remarkable.py` to the mount point.

Everything is documented with `backup_remarkable.py --help`, but here are the
main use-cases:

### Just backup everything

`backup_remarkable.py ~/rm2_mountpoint .`

This will create a folder like `2022-31-12_remarkable` (with the current date)
in the current folder and create a full copy of everything.

### Backup everything and include deleted files, too

`backup_remarkable.py --deleted ~/rm2_mountpoint .`


### List all files that are marked for deletion

`cleanup_remarkable.py ~/rm2_mountpoint`

### Actually delete all files that are marked for deletion

`cleanup_remarkable.py --delete-all ~/rm2_mountpoint`


## Background

When you delete folders or documents from your reMarkable 2, they are first 
placed in the Trash.

When you then delete folders or documents from the Trash, there is a 
difference in behaviour if you have cloud sync enabled (i.e. have an 
account at my.remarkable.com and use it to synchronise your reMarkable's 
content to their cloud storage) or if you don't.

If you do not have cloud sync enabled (e.g. because you don't have an 
account), files are not actually deleted even when you clear the 
Trash. Instead the documents are marked as "deleted" and remain.

This is where `cleanup_remarkable.py` comes in handy: it will list all 
files that are marked as "deleted" and allows you to actually remove them 
from the reMarkable and free space.


## License

MIT.
