#!/usr/bin/env python3
"""Script to create a full backup of your reMarkable 2

The backup script expects that the remarkable is sshfs mounted and
that the webinterface is enabled.

It will then:

1. Create a backup of the status pictures (sleep, power on, etc),
2. Create a backup of all template files,
3. Create a direct copy of all notebooks, PDFs, and EPUBs,
4. Create a PDF export of all notebooks.

See --help parameter to find out how to control the behaviour.
"""
__version__ = '0.2.1'
import datetime
import argparse
import sys
import shutil
import json
import traceback
import inspect
from urllib.request import urlopen
from pathlib import Path

try:
    import rmrl
    import rmrl.constants
except ImportError:
    rmrl = None


VERBOSE = False
QUIET = False


def status(*args, **kwargs):
    if not QUIET:
        print(*args, **kwargs, file=sys.stdout)

def print_info(*args, **kwargs):
    if VERBOSE:
        print(*args, **kwargs, file=sys.stdout)


def print_error(*args, **kwargs):
    print(*args, **kwargs, file=sys.stderr)


def create_backup(args):
    source = Path(args.source).expanduser()
    destination = Path(args.destination).expanduser()

    # verify we're looking at a sshfs mounted remarkable
    if not (source / 'usr' / 'share' / 'remarkable'). is_dir():
        raise RuntimeError(f"{source} does not look like a reMarkable 2 file structure")

    if not destination.exists():
        raise RuntimeError(f"{destination} does not exist")

    # create backup destination folder
    today = datetime.date.today().strftime(args.folder_format)
    destination = destination / today
    destination.mkdir(exist_ok=args.overwrite_target)
    print_info(f"Creating {destination}")

    if not args.skip_images:
        status("Copying status pictures")
        try:
            backup_pictures(source, destination)
        except KeyboardInterrupt:
            status("Cancel")

    if not args.skip_templates:
        status("Copying templates")
        try:
            backup_templates(source, destination)
        except KeyboardInterrupt:
            status("Cancel")

    if not args.skip_files:
        status("Copying notebooks and documents")
        try:
            backup_files(source, destination, args.deleted)
        except KeyboardInterrupt:
            status("Cancel")

    if not args.skip_pdf_export:
        status("Exporting all notebooks and annotated PDFs")
        try:
            export_documents(source, destination, args.deleted)
        except KeyboardInterrupt:
            status("Cancel")


def backup_pictures(source, destination):
    rel_path = Path('usr/share/remarkable')

    source = source / rel_path
    destination = destination / rel_path
    destination.mkdir(exist_ok=True, parents=True)

    for filepath in source.iterdir():
        if filepath.suffix != '.png':
            continue
        target = destination / filepath.name
        print_info(f"Copying {filepath} to {target}")
        shutil.copy(filepath, target)


def backup_templates(source, destination):
    rel_path = Path('usr/share/remarkable/templates')

    source = source / rel_path
    basedestination = destination / rel_path

    paths = [source]
    while len(paths) > 0:
        path = paths.pop(0)
        for filepath in path.iterdir():
            if filepath.is_dir():
                paths.append(filepath)
                continue
            targetpath = filepath.parent.relative_to(source)
            targetpath = basedestination / targetpath
            targetpath.mkdir(exist_ok=True, parents=True)
            target = targetpath / filepath.name
            print_info(f"Copying {filepath} to {target}")
            shutil.copy(filepath, target)


def backup_files(source, destination, includedeleted):
    rel_path = Path('home/root/.local/share/remarkable/xochitl/')

    source = source / rel_path
    destination = destination / rel_path
    destination.mkdir(exist_ok=True, parents=True)

    filesystem = ReFS(source)
    queue = list(filesystem.iterdir())
    while len(queue) > 0:
        node = queue.pop(0)

        queue += node.iterdir()
        if len(node.files) == 0:
            continue
        if node.is_deleted and not includedeleted:
            continue

        print_info(f"Copying {node.path()}/{node.name}")

        for filepath in node.files:
            relpath = filepath.relative_to(node.metadatapath.parent)
            target = destination / relpath
            target.parent.mkdir(exist_ok=True, parents=True)
            print_info(f" ... {filepath.relative_to(node.metadatapath.parent)}")
            shutil.copy(filepath, target)


def export_documents(source, destination, includedeleted):
    rel_path = Path('home/root/.local/share/remarkable/xochitl/')

    templates = source / 'usr' / 'share' / 'remarkable' / 'templates'
    source = source / rel_path
    destination = destination / 'Documents'
    destination.mkdir(exist_ok=True, parents=True)

    filesystem = ReFS(source)

    kwargs = {}
    if rmrl is not None and rmrl.constants is not None:
        rmrl.constants.TEMPLATE_PATH = templates
        if any(n == 'template_path'
               for n in inspect.signature(rmrl.render).parameters):
            kwargs['template_path'] = templates

    queue = list(filesystem.iterdir())
    while len(queue) > 0:
        node = queue.pop(0)
        queue += node.iterdir()

        if not node.has_pages:
            continue

        if node.is_deleted and not includedeleted:
            continue

        targetfn = node.name
        if not targetfn.endswith('.pdf'):
            targetfn += '.pdf'
        target = destination / node.path() / targetfn

        print_info(f"Getting {node.uid} as {target.relative_to(destination)}")
        target.parent.mkdir(exist_ok=True, parents=True)

        if rmrl is not None:
            with open(target, 'wb') as targetfh:
                stream = rmrl.render(str(node.files[0]), **kwargs)
                stream.seek(0)
                targetfh.write(stream.read())

        else:
            url = f'http://10.11.99.1/download/{node.uid}/pdf'

            with urlopen(url) as req:
                with open(target, 'wb') as targetfh:
                    while True:
                        blob = req.read(4096)
                        targetfh.write(blob)
                        if len(blob) < 4096:
                            break


class ReFS:
    """A file remarkable FS file tree"""

    class RootNode:
        """A root node"""
        def __init__(self, filesystem):
            self.filesystem = filesystem
            self.uid = ''
            self.parent = self
            self.parent_uid = ''
            self.files = []
            self.metadata = {}
            self.has_pages = False

            self.name = ''

        def __repr__(self):
            return f'<{type(self).__name__} {self.name}>'

        def iterdir(self):
            for node in self.filesystem.nodes.values():
                if node.parent is self and node is not self:
                    yield node

        def path(self):
            parts = []
            ptr = self.parent
            assert isinstance(ptr, ReFS.RootNode)
            while isinstance(ptr, ReFS.Node):
                parts.insert(0, ptr.name)
                ptr = ptr.parent
            return '/'.join(parts)

    class Node(RootNode):
        """A node in the remarkable FS virtual filesystem"""
        def __init__(self, filesystem, metadatapath, metadata):
            super().__init__(filesystem)
            self.metadatapath = metadatapath
            self.metadata = metadata or json.loads(metadatapath.read_text())
            contentpath = metadatapath.parent / (metadatapath.stem + '.content')
            self.content = json.loads(contentpath.read_text())
            self.uid = metadatapath.stem

            parentpath = metadatapath.parent
            self.files = [fp for fp in parentpath.iterdir()
                          if fp.stem == self.uid and fp.is_file()]
            pagesdir = (parentpath / self.uid)
            if pagesdir.is_dir():
                self.files += list(pagesdir.iterdir())
                self.has_pages = True
            self.parent = None
            self.parent_uid = self.metadata['parent']

            self.name = self.metadata['visibleName']

        @property
        def is_deleted(self):
            return self.metadata.get('deleted', False)

    class Folder(Node): pass

    class Document(Node):
        @property
        def is_notebook(self):
            return self.content.get('fileType', '') == 'notebook'

    def __init__(self, path):
        self.nodes = {}
        self.roots = []

        self._load(path)

    def _load(self, path):
        self.nodes = {}
        self.parents = []

        for filepath in path.iterdir():
            if filepath.suffix != '.metadata':
                continue
            node = self.make_node(filepath)
            self.nodes[node.uid] = node
        self.nodes[''] = ReFS.RootNode(self)
        self.nodes['trash'] = ReFS.RootNode(self)
        self.roots += [self.nodes[''], self.nodes['trash']]

        for node in self.nodes.values():
            node.parent = self.nodes[node.parent_uid]

    def make_node(self, filepath):
        metadatapath = filepath.parent / (filepath.stem + '.metadata')
        metadata = json.loads(metadatapath.read_text())

        if metadata['type'] == 'CollectionType':
            return ReFS.Folder(self, metadatapath, metadata)
        if metadata['type'] == 'DocumentType':
            return ReFS.Document(self, metadatapath, metadata)
        raise RuntimeError(f'Unknown metadata type for {metadatapath}: '
                           + metadata["type"])

    def iterdir(self):
        """Iterate over all roots in the filesystem"""
        for root in self.roots:
            yield root


def parse_args():
    parser = argparse.ArgumentParser()

    parser.add_argument('source',
                        type=str,
                        help='Folder where your remarkable has been mounted '
                             'with sshfs (or similar means)')

    parser.add_argument('destination',
                        type=str,
                        help='Backup destination folder. The backup will be '
                             'created here. '
                             'A new folder with the backup will be created '
                             'there.')

    parser.add_argument('--folder-format',
                        type=str,
                        default='%Y-%m-%d-remarkable',
                        help="Folder name to create for the backup. "
                             "Defaults to '%(default)s'")

    parser.add_argument('--overwrite-target', '-o',
                        action='store_true',
                        default=False,
                        help='Overwrite any existing files in the destination '
                             'folder.')

    parser.add_argument('--quiet', '-q',
                        default=False,
                        action='store_true',
                        help='Do not print any status information except errors.')
    parser.add_argument('--verbose', '-v',
                        default=False,
                        action='store_true',
                        help='Print more information during the backup')

    parser.add_argument('--skip-images', '-I',
                        default=False,
                        action='store_true',
                        help='Do not backup the status images')
    parser.add_argument('--skip-templates', '-T',
                        default=False,
                        action='store_true',
                        help='Do not backup the templates')
    parser.add_argument('--skip-files', '-F',
                        default=False,
                        action='store_true',
                        help='Do not make a direct copy of files and '
                             'documents, i.e. .rm files and such.')
    parser.add_argument('--skip-pdf-export', '-P',
                        default=False,
                        action='store_true',
                        help='Do not create the PDF export of your notebooks '
                             'and annotated PDFs')

    parser.add_argument('--deleted',
                        default=False,
                        action='store_true',
                        help='Included deleted files in the backup. '
                             'By default they are ignored.')


    return parser.parse_args()


def main():
    args = parse_args()

    global QUIET
    QUIET = args.quiet

    global VERBOSE
    VERBOSE = args.verbose

    try:
        create_backup(args)
    except RuntimeError as exc:
        print_error(f"Backup failed: {exc}")
        return -1
    except (OSError, IOError) as exc:
        print_error(f"Backup failed: {exc}")
        return -2
    except Exception as exc:
        callstack = ''.join(traceback.format_tb(exc.__traceback__))
        print_error(f"Surprise error: {exc}\n{callstack}")
        return -3
    return 0

if __name__ == '__main__':
    sys.exit(main())
