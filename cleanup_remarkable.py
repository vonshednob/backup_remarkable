#!/usr/bin/env python3
"""Script to cleanup deleted files from your reMarkable 2

This cleanup script expects that your reMarkable 2 is mounted
(for example through sshfs).

It will then list all files and folders that you deleted from
the Trash can and that have not actually been removed from
your remarkable yet.

By default this script will only list the files that have
been marked for deletion and not clean them up.

See --help for more options.
"""
__version__ = '0.1.0'

import sys
import argparse
import json
import shutil
from pathlib import Path


def find_deleted(basepath):
    """Returns a dict of all documents that have been deleted

    The key is the path to the metadata file, the value the
    dict of that file's parsed content.
    """
    to_delete = {}
    for filepath in basepath.iterdir():
        if filepath.suffix != '.metadata':
            continue

        metadata = json.loads(filepath.read_text())
        if metadata.get('deleted', False):
            to_delete[filepath] = metadata
    return to_delete


def delete_files(paths, mock=True):
    """Deletes all document entries pointed to by ``paths``

    If ``mock`` is ``True``, no deletion happens, but instead the file
    that would be deleted is printed to stdout.

    This will delete all files with the same stem as the given files in
    their respective folder.
    """

    count_deleted = 0
    for basepath in paths:
        stem = basepath.stem
        for target in basepath.parent.iterdir():
            if target.stem != stem:
                continue

            human_readable = target.relative_to(basepath.parent)
            print(f"Deleting {human_readable}")
            if mock:
                continue

            try:
                if target.is_dir():
                    shutil.rmtree(target)
                elif target.is_file():
                    target.unlink()
                else:
                    continue
                count_deleted += 1
            except OSError as exc:
                print(f"Failed to delete {target}: {exc}")

    return count_deleted


def main():
    """Main entry point"""
    args = parse_args()

    source = Path(args.source).expanduser()
    rel_path = Path('home/root/.local/share/remarkable/xochitl/')
    docdir = source / rel_path
    if not docdir.is_dir():
        print(f"{source} does not look like a reMarkable 2 file structure",
              file=sys.stderr)
        return -1

    marked = find_deleted(docdir)

    if args.delete_all:
        confirmed = input(f"Are you sure you want to delete {len(marked)} "
                           "entries? Type 'YES' to confirm: ") == 'YES'
        if not confirmed:
            print("Cancelled")
            return 1

        deleted = delete_files(list(marked.keys()), mock=False)
        if deleted == len(marked):
            return 0
        return -2

    fmt = len(str(len(marked)))
    for nmbr, pair in enumerate(sorted(marked.items(), key=lambda m: m[1]['visibleName'])):
        _, metadata = pair
        print(f"{nmbr+1:>{fmt}}   {metadata['visibleName']}")
    if len(marked) == 0:
        print("No files marked for deletion", file=sys.stderr)

    return 0


def parse_args():
    """Parse the command line arguments"""
    parser = argparse.ArgumentParser()

    parser.add_argument('source',
                        type=str,
                        help='Folder where your remarkable has been mounted '
                             'with sshfs (or similar means)')

    parser.add_argument('--delete-all',
                        action='store_true',
                        default=False,
                        help="Just delete all elements that are marked for deletion")

    return parser.parse_args()

if __name__ == '__main__':
    sys.exit(main())
